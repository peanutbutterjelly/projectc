# SWEN30006 Project C
Project C for SWEN30006 Software Modelling and Design at the University of Melbourne

---

Students (sorted according to student number)

 - Joanna Grace Cho Ern Lee (710094)
 - Shing Sheung Daniel Ip (723508)
 - Nelson Kai Miin Chen (743322)

---

State machine diagram and modifed UML are hosted on draw.io and stored on Google Drive

State Machine Diagram: <https://www.draw.io/>

Modifed UML: <https://www.draw.io/#G0B8P1vCYnkwUyRG9EcjhSUlJHcEE>
